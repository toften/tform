package net.toften.tform;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;


import org.apache.commons.logging.Log;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 * The Transformer class will map a bean with a Velocity template, generating a file.
 * <p>
 * The generated 
 * 
 * @author thomaslarsen
 *
 */
public class Transformer implements TransformerProvider {
	private String id;
	private Properties prop;
	private WriterProvider writerProvider;
	private String[] contextClasses;
	private Log log;

	private void doVelocity(Map<String, Object> contextBeans, Object bean, File templateFile) throws ClassNotFoundException, IOException {
		VelocityContext context = new VelocityContext();
		context.put("properties", prop);
		context.put("bean", bean);
		
		// Add context beans
		for (Entry<String, Object> contextBean : contextBeans.entrySet()) {
			context.put(contextBean.getKey(), contextBean.getValue());
		}
		
		if (contextClasses != null) {
			for (String className : contextClasses) {
				Class<?> clazz = Class.forName(className);
				context.put(clazz.getSimpleName(), clazz);
				logMessage("Adding " + clazz.getName() + " as " + clazz.getSimpleName());
			}
		}
		
		try {
			Writer writer = writerProvider.getWriter(prop, bean);
			
			Reader templateReader = new FileReader(templateFile);
			
	    	Velocity.evaluate(context, writer, id, templateReader);
	        
	    	templateReader.close();
	    	
	    	writer.flush();
	    	writer.close();
		} catch (WriterSkipException e) {
			logMessage("Bean skipped");
		}
	}
	
	public void setLog(Log log) {
		this.log = log;
		
		writerProvider.setLog(log);
	}
	
	private void logMessage(String message) {
		if (log != null && log.isInfoEnabled())
			log.info(message);
		else
			System.out.println(message);
	}
	
	public void transform(Map<String, Object> contextBeans, Object contextBean, File templateFile) throws Exception {
		Velocity.init();
		
		if (contextBean instanceof Collection<?>) {
			logMessage("Bean is collection");
			Collection<?> beans = (Collection<?>) contextBean;
			logMessage(beans.size() + " beans found in collection");
			for (Object bean : beans) {
				doVelocity(contextBeans, bean, templateFile);
			}
		} else if (contextBean.getClass().isArray()) {
			logMessage("Bean is array");
			Object[] beans = (Object[]) contextBean;
			logMessage(beans.length + " beans found in array");
			for (Object bean : beans) {
				doVelocity(contextBeans, bean, templateFile);
			}
		} else {
			logMessage("Bean is singleton");
			doVelocity(contextBeans, contextBean, templateFile);
		}
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	public void setWriterProvider(WriterProvider writerProvider) {
		this.writerProvider = writerProvider;
	}

	public void setContextClasses(String[] contextClasses) {
		this.contextClasses = contextClasses;
	}
}