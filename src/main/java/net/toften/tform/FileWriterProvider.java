package net.toften.tform;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;

public class FileWriterProvider implements WriterProvider {
	public static final String FILENAME_FIELD_PROPERTY = "filenamepath";
	public static final String FILEPATH_FIELD_PROPERTY = "filepathpath";
	public static final String FILEEXT_FIELD_PROPERTY = "fileextpath";
	public static final String FILENAME_CAPITALISE_PROPERTY = "filecapitalize";
	public static final String DEFAULT_FILEEXT_PROPERTY = "defaultfileext";
	public static final String DEFAULT_FILENAME_PROPERTY = "defaultfilename";
	public static final String DEFAULT_FILEPATH_PROPERTY = "defaultfilepath";
	public static final String TARGET_PROPERTY = "outputPath";
	
	public static final String DEFAULT_FILEPATH = System.getProperty("file.separator");
	private Log log;
	
	public Writer getWriter(Properties prop, Object bean) throws IOException, IllegalStateException, WriterSkipException {
		String filenamePathProperty = prop.getProperty(FILENAME_FIELD_PROPERTY);
		String filepathProperty = prop.getProperty(FILEPATH_FIELD_PROPERTY);
		String fileextProperty = prop.getProperty(FILEEXT_FIELD_PROPERTY);
		Boolean capitaliseFile = new Boolean(prop.getProperty(FILENAME_CAPITALISE_PROPERTY, "false"));
		String outputFolder = prop.getProperty(TARGET_PROPERTY);

		String filename, filepath, fileext;
		try {
			filename = (String) PropertyUtils.getNestedProperty(bean, filenamePathProperty); 
		} catch (Exception e) {
			filename = prop.getProperty(DEFAULT_FILENAME_PROPERTY);
		}

        if (capitaliseFile)
            filename = filename.substring(0, 1).toUpperCase() + filename.substring(1);

        try {
			fileext = (String) PropertyUtils.getNestedProperty(bean, fileextProperty); 
		} catch (Exception e) {
			fileext = prop.getProperty(DEFAULT_FILEEXT_PROPERTY);
		}

		try {
			filepath = (String) PropertyUtils.getNestedProperty(bean, filepathProperty);
		} catch (Exception e) {
			filepath = prop.getProperty(DEFAULT_FILEPATH_PROPERTY);
			if (filepath == null)
				filepath = DEFAULT_FILEPATH;
		}

		logMessage("Filename is " + filename);
		logMessage("Filepath is " + filepath);

		if (filename == null)
			throw new IllegalStateException("Filename not specified");
		if (filename.equals(""))
			throw new WriterSkipException();
		if (filepath == null)
			throw new IllegalStateException("Filepath not specified");

		File fileDestDir = new File(outputFolder + System.getProperty("file.separator") + filepath);
		fileDestDir.mkdirs();
		File destFile = new File(fileDestDir, filename + (fileext != null ? ("." + fileext) : ""));
        destFile.delete();
        destFile.createNewFile();
		
        return new BufferedWriter(new FileWriter(destFile));
	}

	public void setLog(Log log) {
		this.log = log;
	}
	
	private void logMessage(String message) {
		if (log != null && log.isInfoEnabled())
			log.info(message);
		else
			System.out.println(message);
	}
}