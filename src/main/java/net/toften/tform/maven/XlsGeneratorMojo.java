package net.toften.tform.maven;

import java.io.File;

import jxl.Workbook;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @author thomas
 *
 * @goal xls
 * @phase generate-sources
 */
public class XlsGeneratorMojo extends TransformerMojo {
	/**
	 * @parameter
	 */
	private File xlsFile;

	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			Workbook workbook = Workbook.getWorkbook(xlsFile);
			
			transform(workbook);
		} catch (Exception e) {
			throw new MojoExecutionException("Failed", e);
		}
	}
}