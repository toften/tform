package net.toften.tform.maven;

import java.util.Properties;

import org.apache.maven.plugin.logging.Log;

public interface ObjectFactory {
    Object instantiate(Properties prop, Log log) throws Exception;
}
