package net.toften.tform.maven;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.tform.FileWriterProvider;
import net.toften.tform.TransformerProvider;
import net.toften.tform.WriterProvider;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.project.MavenProject;

public abstract class TransformerMojo extends AbstractMojo {
	/**
	 * The path where the generated file(s) should be written to.
	 * 
	 * @parameter default-value="${project.build.directory}/generated-sources/${plugin.artifactId}"
	 * @required
	 */
	private File outputPath;
	
	/**
	 * If the generated file(s) should be added as a source to the maven project.
	 * <p>
	 * Adding the files as a source will include them in the compile phase of the project.
	 * 
	 * @parameter default-value="false"
	 */
	private boolean addOutputToSource;
    
	/**
	 * The Velocity template file to use
	 * 
	 * @parameter
	 * @required
	 */
	private File template;
	
	/**
	 * The ID of the transformation.
	 * <p>
	 * The ID is passed to the Velocity engine as the <code>logTag</code>
	 * 
	 * @parameter
	 */
	private String id;
	
	/**
	 * The object path into the extracted bean that will be passed into the Velocity engine as the <code>bean</code>
	 * property
	 * 
	 * @parameter
	 */
	private String beanPath;
	
	/**
	 * The {@link WriterProvider} class name to output the generated files to
	 * 
	 * @parameter default-value="net.toften.tform.SystemOutWriterProvider"
	 * @required
	 */
	private String writer;
	
	/**
	 * The {@link TransformerProvider} class name.
	 * 
	 * @parameter default-value="net.toften.tform.Transformer"
	 * @required
	 */
	private String transformer;
	
	/**
	 * Properties to provide to the {@link #writer}
	 * 
	 * @parameter
	 */
	private Properties writerProperties;
	
	/**
	 * Array of classes to add to the Velocity context.
	 * <p>
	 * The {@link Class} will be added using the {@link Class#getSimpleName() simple name} as the property key.
	 * 
	 * @parameter
	 */
	private String[] staticContext;
	
	/**
	* Project instance, used to add new source directory to the build.
	* @parameter default-value="${project}"
	* @required
	* @readonly
	*/
	private MavenProject project;
	
	private Map<String, Object> contextBeans = new HashMap<String, Object>();
	
	protected void addToContextBeanMap(String beanName, Object bean) {
		contextBeans.put(beanName, bean);
	}

	protected void transform(Object rootBean) throws Exception {
		Object bean;
		
		// Adjust bean
		if (beanPath != null) {
			bean = PropertyUtils.getNestedProperty(rootBean, beanPath);
		} else {
			bean = rootBean;
		}
		
		// Add root to bean map
		addToContextBeanMap("root", rootBean);
		
		// Add project to bean map
		addToContextBeanMap("project", project);

		// Adjust output path
		File adjustedPath = new File(outputPath.getAbsolutePath() + System.getProperty("file.separator") + id);
		if (writerProperties != null)
			writerProperties.put(FileWriterProvider.TARGET_PROPERTY, adjustedPath.getAbsolutePath());

		Class<? extends TransformerProvider> transformerClass = (Class<? extends TransformerProvider>) Class.forName(transformer);
		TransformerProvider t = transformerClass.newInstance();
		
		t.setId(id);
		Class<? extends WriterProvider> writerClass = (Class<? extends WriterProvider>) Class.forName(writer);
		t.setWriterProvider(writerClass.newInstance());
		t.setProp(writerProperties);
		t.setContextClasses(staticContext);

		t.transform(contextBeans, bean, template);

		if (addOutputToSource)
			project.addCompileSourceRoot(adjustedPath.getAbsolutePath());
	}
}