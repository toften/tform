package net.toften.tform.maven;

import java.io.File;

import javax.xml.bind.JAXBContext;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @author thomas
 *
 * @goal xsd
 * @phase generate-sources
 */
public class JaxbGeneratorMojo extends TransformerMojo {
	/**
	 * @parameter
	 */
	private File xmlFile;
	
	/**
	 * @parameter
	 */
	private String jaxbPath;

	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
//			JAXBContext jc = JAXBContext.newInstance(jaxbPath, getClass().getClassLoader());
            JAXBContext jc = JAXBContext.newInstance(jaxbPath);
			
			transform(jc.createUnmarshaller().unmarshal(xmlFile));
		} catch (Exception e) {
			throw new MojoExecutionException("Failed", e);
		}
	}
}