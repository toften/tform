package net.toften.tform;

import java.io.File;
import java.util.Map;
import java.util.Properties;

/**
 * Implementation of a transformer using a template file. 	
 * 
 * @author tlarsen
 *
 */
public interface TransformerProvider {
	void transform(Map<String, Object> contextBeans, Object contextBean, File templateFile) throws Exception;
	
	void setId(String id);
	
	void setProp(Properties prop);
	
	void setWriterProvider(WriterProvider writerProvider);
	
	void setContextClasses(String[] contextClasses);
}
