package net.toften.tform;

import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.logging.Log;

public interface WriterProvider {
	Writer getWriter(Properties prop, Object bean) throws IOException, IllegalStateException, WriterSkipException;
	
	void setLog(Log log);
}
