package net.toften.tform.ant;

import java.util.ArrayList;
import java.util.List;

public class StaticContext {
	public class AntProperty {
		public void addText(String text) {
			contextClasses.add(text);
		}
	}
	
	private List<String> contextClasses = new ArrayList<String>();

	public AntProperty createClass() {
		return new AntProperty();
	}

	public String[] getArray() {
		return contextClasses.toArray(new String[0]);
	}
}
