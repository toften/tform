package net.toften.tform.ant;


public class FileWriterProperties extends WriterProperties {
	public class AntProperty {
		private String key;

		public AntProperty(String key) {
			this.key = key;
		}
		
		public void addText(String value) {
			setProperty(key, value);
		}
	}

	public static final String FILENAME_FIELD_PROPERTY = "filenamepath";
	public static final String FILEPATH_FIELD_PROPERTY = "filepathpath";
	public static final String FILEEXT_FIELD_PROPERTY = "fileextpath";
	public static final String FILENAME_CAPITALISE_PROPERTY = "filecapitalize";
	public static final String DEFAULT_FILEEXT_PROPERTY = "defaultfileext";
	public static final String DEFAULT_FILENAME_PROPERTY = "defaultfilename";
	public static final String DEFAULT_FILEPATH_PROPERTY = "defaultfilepath";

	public AntProperty createFilenamepath() {
		return new AntProperty(FILENAME_FIELD_PROPERTY);
	}
	
	public AntProperty createFilepathpath() {
		return new AntProperty(FILEPATH_FIELD_PROPERTY);
	}
	
	public AntProperty createFileextpath() {
		return new AntProperty(FILEEXT_FIELD_PROPERTY);
	}
	
	public AntProperty createFilecapitalize() {
		return new AntProperty(FILENAME_CAPITALISE_PROPERTY);
	}
	
	public AntProperty createDefaultfileext() {
		return new AntProperty(DEFAULT_FILEEXT_PROPERTY);
	}
	
	public AntProperty createDefaultfilename() {
		return new AntProperty(DEFAULT_FILENAME_PROPERTY);
	}
	
	public AntProperty createDefaultfilepath() {
		return new AntProperty(DEFAULT_FILEPATH_PROPERTY);
	}
}
