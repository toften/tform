package net.toften.tform.ant;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.toften.tform.FileWriterProvider;
import net.toften.tform.TransformerProvider;
import net.toften.tform.WriterProvider;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.tools.ant.Task;

public class TransformerTask extends Task {

	/**
	 * The Velocity template file to use
	 * 
	 * @parameter
	 * @required
	 */
	private File template;
	/**
	 * The ID of the transformation.
	 * <p>
	 * The ID is passed to the Velocity engine as the <code>logTag</code>
	 * 
	 * @parameter
	 */
	private String id;
	/**
	 * The object path into the extracted bean that will be passed into the Velocity engine as the <code>bean</code>
	 * property
	 * 
	 * @parameter
	 */
	private String beanPath;
	/**
	 * The {@link WriterProvider} class name to output the generated files to
	 * 
	 * @parameter default-value="net.toften.tform.SystemOutWriterProvider"
	 * @required
	 */
	private String writer;
	/**
	 * The {@link TransformerProvider} class name.
	 * 
	 * @parameter default-value="net.toften.tform.Transformer"
	 * @required
	 */
	private String transformer;
	/**
	 * Properties to provide to the {@link #writer}
	 * 
	 * @parameter
	 */
	private Properties writerProperties;
	/**
	 * Array of classes to add to the Velocity context.
	 * <p>
	 * The {@link Class} will be added using the {@link Class#getSimpleName() simple name} as the property key.
	 * 
	 * @parameter
	 */
	private StaticContext staticContext;
	/**
	 * The path where the generated file(s) should be written to.
	 * 
	 * @parameter default-value="${project.build.directory}/generated-sources/${plugin.artifactId}"
	 * @required
	 */
	private File outputPath;

	private Map<String, Object> contextBeans = new HashMap<String, Object>();

	protected void addToContextBeanMap(String beanName, Object bean) {
		contextBeans.put(beanName, bean);
	}

	protected void transform(Object rootBean) throws Exception {
		Object bean;
		
		// Adjust bean
		if (beanPath != null) {
			bean = PropertyUtils.getNestedProperty(rootBean, beanPath);
		} else {
			bean = rootBean;
		}
		
		// Add root to bean map
		addToContextBeanMap("root", rootBean);
		
		// Adjust output path
		File adjustedPath = new File(outputPath.getAbsolutePath() + System.getProperty("file.separator") + id);
		if (writerProperties != null)
			writerProperties.put(FileWriterProvider.TARGET_PROPERTY, adjustedPath.getAbsolutePath());
	
		Class<? extends TransformerProvider> transformerClass = (Class<? extends TransformerProvider>) Class.forName(transformer);
		TransformerProvider t = transformerClass.newInstance();
	
		Class<? extends WriterProvider> writerClass = (Class<? extends WriterProvider>) Class.forName(writer);
		t.setWriterProvider(writerClass.newInstance());
		t.setId(id);
		t.setProp(writerProperties);
		t.setContextClasses(staticContext.getArray());
		
		t.transform(contextBeans, bean, template);
	}

	public void setOutputpath(File outputPath) {
		this.outputPath = outputPath;
	}

	public void setTemplate(File template) {
		this.template = template;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setBeanpath(String beanPath) {
		this.beanPath = beanPath;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}
	
	public FileWriterProperties createFilewriterproperties() {
		FileWriterProperties fwp = new FileWriterProperties();
		this.writerProperties = fwp;
		
		return fwp;
	}
	
	public StaticContext createStaticcontext() {
		this.staticContext = new StaticContext();
		
		return this.staticContext;
	}
}