package net.toften.tform.ant;

import java.io.File;

import jxl.Workbook;

public class XlsTask extends TransformerTask {
	/**
	 * @parameter
	 */
	private File xlsFile;

	public void execute() {
		try {
			Workbook workbook = Workbook.getWorkbook(xlsFile);
			
			transform(workbook);
		} catch (Exception e) {
			
		}
	}

	public void setXlsfile(File xlsFile) {
		this.xlsFile = xlsFile;
	}
}
