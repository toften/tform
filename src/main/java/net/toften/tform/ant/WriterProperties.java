package net.toften.tform.ant;

import java.util.Properties;

public class WriterProperties extends Properties {

	public static final String TARGET_PROPERTY = "outputPath";
	
	public WriterProperties() {
		// TODO Auto-generated constructor stub
	}
	
	public void setOutputpath(String outputpath) {
		setProperty(TARGET_PROPERTY, outputpath);
	}
}
