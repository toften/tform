package net.toften.tform;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.logging.Log;

public class SystemOutWriterProvider implements WriterProvider {
	private Log log;

	public Writer getWriter(Properties prop, Object bean) {
	    logMessage("New System Output created");
	    
		return new OutputStreamWriter(System.out);
	}
	
	public void setLog(Log log) {
		this.log = log;
	}
	
	private void logMessage(String message) {
		if (log != null && log.isInfoEnabled())
			log.info(message);
		else
			System.out.println(message);
	}
}